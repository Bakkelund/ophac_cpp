# ophac-cpp

This python module contains a c++ extension to `ophac` for
performance enhancements, and should be accessed 
through the API of `ophac`.

See the documentation of [ophac](https://pypi.org/project/ophac/) for details.

## Licensing

The software in this package is released under the [GNU Lesser General Public License](https://www.gnu.org/licenses/lgpl-3.0.en.html).

## Installation

This package is supposed to install automatically when installing `ophac`. However, if you suspect that something has gone wrong, you can install this package manually via

```python
python -m pip install ophac-cpp [--user]
```
